// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Archer.generated.h"

UCLASS()
class HUNTER_API AArcher : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AArcher();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Kamera")
	class USpringArmComponent* FollowCameraDistance;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Kamera")
	class UCameraComponent* FollowCamera;

	UPROPERTY(VisibleAnywhere, Category = "movemenent :: Crouch")
	bool isChrouching;

	UPROPERTY(VisibleAnywhere, Category = "movemenent :: Running")
	float SpeedMultiplyer;

	UPROPERTY(VisibleAnywhere, Category = "movemenent :: Running")
	bool isRuning;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void MoveForward(float axis);
	void MoveRight(float axis);
	void ToggleCrouch();
	void Sprinting();
	void StopSprinting();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
