// Fill out your copyright notice in the Description page of Project Settings.


#include "Archer.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/InputComponent.h"
// Sets default values
AArcher::AArcher()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	// set Speed Multiplyer *2
	SpeedMultiplyer = 2.0f;

	// Initialise the can crouch property  
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->GetNavAgentPropertiesRef().bCanCrouch = true;

	FollowCameraDistance = CreateDefaultSubobject<USpringArmComponent>(TEXT("FollowCameraDistance"));
	FollowCameraDistance->SetupAttachment(RootComponent);
	FollowCameraDistance->TargetArmLength = 300.0f;
	FollowCameraDistance->bUsePawnControlRotation = true;

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(FollowCameraDistance, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false;
}

// Called when the game starts or when spawned
void AArcher::BeginPlay()
{
	Super::BeginPlay();
	
}

void AArcher::MoveForward(float axis)
{
	if (Controller != nullptr && axis != 0.0f) {
		FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
		AddMovementInput(Direction, axis);
	}
}

void AArcher::MoveRight(float axis)
{
	if (Controller != nullptr && axis != 0.0f) {
		FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
		AddMovementInput(Direction, axis);
	}
}

void AArcher::ToggleCrouch()
{
	if (CanCrouch() == true)
	{
		isChrouching = true;
		UE_LOG(LogTemp, Warning, TEXT("Crouch = true"));
		Crouch();
	}
	else {
		isChrouching = false;
		UE_LOG(LogTemp, Warning, TEXT("Crouch = false"));
		UnCrouch();
	}
}

void AArcher::Sprinting()
{
	GetCharacterMovement()->MaxWalkSpeed *= SpeedMultiplyer;
	FollowCameraDistance->TargetArmLength = 400.0f;
	FollowCamera->FieldOfView = 100.0f;
	
}

void AArcher::StopSprinting()
{
	GetCharacterMovement()->MaxWalkSpeed /= SpeedMultiplyer;
	FollowCameraDistance->TargetArmLength = 300.0f;
	FollowCamera->FieldOfView = 90.0f;
}

// Called every frame
void AArcher::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AArcher::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent)
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	// handeling Movemenent
	PlayerInputComponent->BindAxis("MoveForward", this, &AArcher::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AArcher::MoveRight);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);

	// handeling Action Input
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AArcher::Sprinting);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AArcher::StopSprinting);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &AArcher::ToggleCrouch);
}

